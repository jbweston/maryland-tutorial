Definition of generalized eigenproblem:
  kwant/physics/leads.py
    setup_linsys:360-382
Definition of linear system to solve:
  kwant/solvers/common.py
    _make_linear_sys:234-235,246
